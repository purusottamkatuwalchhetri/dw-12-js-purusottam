// make a arrow function that takes a input and 
// return true if the string starts with Bearear else return false
export let fun1 = (value) =>{
    let check = value.startsWith("Bearear");
    return check;
 }