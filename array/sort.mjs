//string sorting

// let ar1=[10,9];
// let ar2=ar1.sort();//[10,9],as in js it compares first character
// console.log(ar2);
// console.log(ar1);

// let ar1=["a","c"];
// let ar2=ar1.sort().reverse();           /*let ar2=ar1.sort();//["a","c"]
                               //  let ar3=ar2.reverse();//["c","a"]  */
// console.log(ar2);

let ar1=["a","b","c","A"];//in alphabets capital letter sorts first than small letters
let ar2=ar1.sort();
console.log(ar2);