 //             0   ,  1   ,  2
let names = ["ram","hari",1000,true];//array is used to store multiple values of different type


//indexing

console.log(names);
console.log(names[1]);//hari as index of hari is 1 
console.log(names[2]);//1000
console.log(names[0]);//ram

//changing elements of array

names[2]=200;
names[3]=false;
names[0]="shyam"; 
console.log(names);

//delete the elements of array

delete names[1];
console.log(names);//[shyam ,,200,false]