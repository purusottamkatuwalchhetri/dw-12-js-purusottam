// let info = ["kashyap",21,false]
let info = {name:"kashyap",age:21,isMarried: false}; //object= value with info
//getting whole object
console.log(info);
//getting object element
console.log(info.name);
console.log(info.age);
console.log(info.isMarried);
//changing object element
info.age=30;
console.log(info);

//deleting
delete info.name;
console.log(info);

