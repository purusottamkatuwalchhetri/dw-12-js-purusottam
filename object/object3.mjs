let obj1 ={
    name:"nitan",
    age:29
}
//converting object into array
let keysArr = Object.keys(obj1);//[ 'name', 'age' ]
console.log(keysArr);

let valuesArr = Object.values(obj1);//[ 'nitan', 29 ] 
console.log(valuesArr);

let prArr = Object.entries(obj1);//[ [ 'name', 'nitan' ], [ 'age', 29 ] ]
console.log(prArr)