let products = [
    {
      id: 1,
      title: "Product 1",
      category: "electronics",
      price: 5000,
      description: "This is description and Product 1",
      discount: {
        type: "other",
      },
    },
    {
      id: 2,
      title: "Product 2",
      category: "cloths",
      price: 2000,
      description: "This is description and Product 2",
      discount: {
        type: "other",
      },
    },
    {
      id: 3,
      title: "Product 3",
      category: "electronics",
      price: 3000,
      description: "This is description and Product 3",
      discount: {
        type: "other",
      },
    },
  ];
// find the array of id ie  output must be [1,2,3]





let pricee3000=products.filter((value,i)=>{
    if(value.price >= 3000 ){
        return true;
    }
});
// console.log(price3000);


//if filter and map are used simultaneously then always use filter first

let product3000 =products.filter((value,i)=>
{
    if(value.price >= 3000)
    return true;

}).map((value,i)=>{
    return value.title;
})
// console.log(product3000)



// find those array of title whose price does not equal to 5000 ==> ["product 2","product 3"]

let price5000=products.filter((value,i)=>
{
    if(value.price !== 5000)
    return true;
}).map((value,i)=>{
    return value.title;
})
// console.log(price5000);


//find those array of title whose price equal to 3000 

let price3000=products.filter((value,i)=>{
    if(value.price===3000)
    return true
}).map((value,i)=>{
    return value.category;
})
console.log(price3000)
//map is used to modify elements whereas filter is used to filter the elements

