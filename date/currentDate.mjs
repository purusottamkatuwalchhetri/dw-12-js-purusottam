let currentDate = new Date();

console.log(currentDate)
//iso format 2024-01-05T10:39:52.274Z
//iso format yyyy-mm-ddThh:mm:ss
//must have 4 digit in year
//must have 2 digit in month and so on 

//date and time in readable format

let dateTime = new Date().toLocaleString();
console.log(dateTime);

let date = new Date().toLocaleDateString();
console.log(date)

let time = new Date().toLocaleTimeString();
console.log(time)