// javascript is a synchronous language=top to bottom
//js is a single threaded language=one execution at a time
// console.log("hello world");
//console ma always put data

/* data-type 
string = always quoted with ""
number 1,2,3 
boolean   true,false
*/

/* operation = +,-,*,/
console.log(1/2);
 */

/* string
"a"
'a'
`a`
demerits
    we cannot use double quotes inside double quotes
    we cannot use single quote inside single quotes
    we cannot use backtick inside backtick
    
 */


/* + operator
only + operator can be performed in string and number both 
if two numbers are added it sum and produce output in number i.e.1+2 = 3
if two string are added it concatenation and produce output in string i.e. "a"+"b" = "ab"
if there is war between string and number string always wins
always two operations ae performed
"1"+"1"="11"
1+"1"="11" string wins

1+2+"3"+4 //left to right single operation and string wins
=>3+"3"+4
=>"33"+4
="334"
*/

// NaN => Not a Number
// it will generate when we use -,/,* mathematical operation at string
// console.log("a"-"b")

//variable
// let age =29;
// let address ="pokhara";

// age=30;
// let country ="nepal";

// we cannot define same variable twice.

// no need semi colon in js like c and c++


// data or variable

// let name ="kashyap";
// console.log(name); 
// console.log(school);//wrong because school variable is not defined
//we cannot call variable if it is not define

// string literals => `` (backtick)=> template literals
// use ${} inside backtick= is used to do javascript operations like add subtract etc
// let num1 =1;
// let num2 =2;

// let message =`num1 is ${num1} and num2 is ${num2} `;
// console.log(message);

// let sum=`the sum of 1 and 2 is ${1+2}`;
// console.log(sum);

// let name="kashyap";
// let surname="chhetri";
// let fullname=`my name is ${name} ${surname}`;
// console.log(fullname);



//truthy and falsy value

// if empty => false
// else => true

// let a = !!"kashyap";
// console.log(a);



// ===,<,>,<=,>=   for comparison
// console.log(1===1);
// console.log(1===2);
// console.log(1<2);
// console.log(4>=4);



//not
//if true => false
//if false => true
//  "!!" converts other into boolean
// "!" converts true into false and viceversa

// let a ="true";
// if(!a){
//     console.log("i am shyam")

// }else{
//     console.log("i am ram");
// }


// let name="rama";
// if(!name)
// {
//     console.log("a");
// }
// else{
//     console.log("b");
// }


//function
// define function
      // a function   will not execute unless it is called
// call function
// pass value to function
// return
// arrow function => always use arrow function

// let fun1 = function () //function define
// {
// console.log("i am a function");
// }
// console.log("a");//a
// fun1();// iam a function
// console.log("b");//b


//arrow function
let fun1 = ()=> { 
console.log("arrow func ma only syntax change garni other all same")
}
fun1();



    