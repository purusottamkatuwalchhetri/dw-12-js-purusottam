//array,object (are non primitive )
//non primitive
//=== in case of non primitive ,it checks whether the memory address is same or not

// let a=[1,2];
// let b=[1,2];
// let c=a;

// console.log(a===b)//false
// console.log(a===c)//true


let ar1=[5,9];
let ar2=[2,3];
let ar3=ar1;

ar1.push(7);
ar2.push(4);

console.log(ar1)//[5,9,7]
console.log(ar2)//[2,3,4]
console.log(ar3)//[5,9,7]


//the typeof non primitive is object